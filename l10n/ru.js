OC.L10N.register(
    "collectives",
    {
    "Add emoji" : "Добавить эмодзи",
    "Restore" : "Восстановить",
    "Delete permanently" : "Удалить навсегда",
    "Cancel" : "Отмена",
    "Delete page" : "Удалить страницу",
    "Page deleted" : "Страница удалена",
    "Could not delete the page" : "Не удалось удалить страницу",
    "Could not find the Page." : "Не удалось найти страницу.",
    "Restore this version" : "Восстановить эту версию",
    "Title" : "Название",
    "Done" : "Готово",
    "Add a page" : "Добавить страницу",
    "Current version" : "Текущая версия",
    "_%n byte_::_%n bytes_" : ["%n байт","%n байта","%n байт"],
    "New Page" : "Новая страница",
    "Please ask the administrator to enable these apps." : "Пожалуйста, попросите администратора включить эти приложения."
},
"nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;");
