OC.L10N.register(
    "collectives",
    {
    "Collectives" : "Kollektiv",
    "Collectives - Page content" : "Kollektiv - sidoinnehåll",
    "in page {page} from collective {collective}" : "i sida {sida} från kollektiv {kollektiv}",
    "Collectives - Pages" : "Kollektiv - Sidor",
    "in Collective {collective}" : "i kollektiv {kollektiv}"
},
"nplurals=2; plural=n != 1;");
